enum class ObstacleType{
    High,
    Low,
    LowWithSecond,
    None
}
enum class ActionType{
    Jump,
    Duck,
    Idle
}

data class Sample(val speed: Int, val distance: Int, val obstacleType: ObstacleType, val actionType: ActionType, val success: Boolean)