import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgcodecs.Imgcodecs
import java.awt.color.ColorSpace
import java.awt.image.BufferedImage
import java.awt.image.ColorConvertOp
import java.awt.image.DataBufferByte
import java.io.File
import java.io.IOException

fun loadImage(file: File): Mat = Imgcodecs.imread(file.absolutePath, Imgcodecs.IMREAD_GRAYSCALE)
        ?: throw IOException("Couldn't load image: " + file.absolutePath)

fun BufferedImage.toOpenCvMat(): Mat {

    val colorSpace = ColorSpace.getInstance(ColorSpace.CS_GRAY)
    val converter = ColorConvertOp(colorSpace, null)
    val grayImage = converter.filter(this, null)

    val mat = Mat(grayImage.height, grayImage.width, CvType.CV_8UC1)
    val data = (grayImage.raster.dataBuffer as DataBufferByte).data
    mat.put(0, 0, data)
    return mat
}